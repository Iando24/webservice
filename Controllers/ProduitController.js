const jspon = require('jspon')
const fs = require('fs')
const produit = require('../Models/Produit')


exports.create = (req, res) => {
    var reqBody = jspon.stringify(req.body)

    fs.rename(req.file.path, file, (err) => {
        if(err){
            console.lgo('Erreur')
            res.send(500)
        }else{
            res.json({
                message: 'Success',
                filename: req.file.filename
            })
        }
    })
    
    if(!req.body) {
        return res.status(400).send({
            message: "Contenue vide ou manquant"
        });
    }

    if(!req.files){
        return res.status(400).send({
            message: "Images required"
        })
    }

    const newProduit = new produit({
        libele: jspon.parse(reqBody).libele,
        prix: jspon.parse(reqBody).prix,
        quantite: jspon.parse(reqBody).quantite,
        type: jspon.parse(reqBody).type,
        imagePath: req.files.filename
    })

    newProduit.save()
    .then(data => {
        res.send(data)
    }).catch(err => {
        res.status(500).send({
            message: err.message
        })
    })

    const storage = new Grid
}

exports.findAll = (req, res) => {
    produit.find()
    .then(produits => {
        res.send(produits);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
}

exports.findOne = (req, res) => {
    produit.findById(req.params.libele)
    .then(data => {
        if(!data){
            return res.status(404).send({
                message: "Erreur"
            })
        }
        res.send(data)
    }).catch(err => {
        if(err.kind === 'ObjectId'){
            return res.status(404).send({
                message: "Erreur"
            })
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.libele
        });
    })
}

exports.update = (req, res) => {
    var reqBody = jspon.stringify(req.body)

    if(!req.body) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    produit.findByIdAndUpdate(req.params.libele, {
        libele: jspon.parse(reqBody).libele,
        prix: jspon.parse(reqBody).prix,
        quantite: jspon.parse(reqBody).quantite,
        type: jspon.parse(reqBody).type
    }, { new: true})
    .then(newProduit => {
        if(!newProduit) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.libele
            });
        }
        res.send(newProduit);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.libele
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.libele
        });
    })
}

exports.delete = (req, res) => {
    produit.findByIdAndRemove(req.params.libele)
    .then(data => {
        if(!data) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.libele
            });
        }
        res.send({message: "Produit deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.libele
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.libele
        });
    })
}