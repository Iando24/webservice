const mongoose = require('mongoose')

const produitModel = mongoose.Schema({
    libele: {
        type: String,
        required: '{PATH} is required'
    },
    prix: {
        type: Number,
        required: '{PATH} is required'
    },
    quantite: {
        type: Number,
        required: '{PATH} is required'
    },
    type: {
        type: String,
        required: '{PATH} is required'
    },
    image: {
        type: String,
        required: '{$PATH} is required'
    }
},{
    timestamps: true
})

module.exports = mongoose.model('produit', produitModel, 'produit')