const produit = require('../../Controllers/ProduitController')
const multer = require('multer')

module.exports = (app) => {
    app.post('/produit', produit.create)
    app.get('/produit',produit.findAll)
    app.get('/produit/:libele', produit.findOne)
    app.put('/produit/:libele',produit.update)
    app.delete('/produit/:libele',produit.delete)
}